# SFML Tetris C++ Project
### Team_25
##### Aglitoiu Marius
##### Balogh David
##### Teposu Paula
##### Cojocaru Rares

## Project Technical Details
### Used IDE
#### IntelliJ cLion

### SFML Istallation Details

#### MacOs
##### `homebrew install`
##### Then reload cmake project

#### Windows
##### Download `SFML` bundle for `minGW 32bits`  copy folder `SFML-2.5.1` to `C:\\`
##### Install `minGW 32bits`
###### https://www.jetbrains.com/help/clion/quick-tutorial-on-configuring-clion-on-windows.html#MinGW
###### Open `File->Options->Build,..->Toolchains` and remove `VisualStudio` option and add `MinGW` option
##### Make sure this repo is the last version from bitbucket
##### Right-click CMakeLists.txt and `reload cmake project`

