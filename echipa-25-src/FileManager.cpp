//
// Created by Aglitoiu Marius on 19/11/2020.
//

#include "FileManager.h"
#include <sstream>

const char* fileName="Tetris++_players.txt";
const char* tempFile="temp_file.txt";

FileManager::FileManager() {
    this->m_file.open(fileName, std::fstream::in | std::fstream::out | std::fstream::app);
    if (!m_file.is_open()) {
        std::cout << "Unable to open this file!!\n";
        return;
    } else {
        std::cout << "File opened!!\n";
        m_file.clear();
    }

}

void FileManager::AddPlayer(const Player& player) {
    if (m_file.is_open()) {
        if (this->Is_empty()) {
            std::cout << "This is a new player";
            m_file.clear();
            m_file.seekp(SEEK_SET);
        }
        else
            {
                m_file.clear();
                m_file.seekp(SEEK_END);
            }

            m_file << player.GetId() << "  Nume:" << player.GetPlayerName() << "   Score: " << player.GetScore() << "   GamesPlayed: "
                   << player.GetGamesPlayed() << "  MaxScore:" << player.GetMaxScore() << "\n";
            m_file.clear();

       }
    else {
        std::cout << "Unable to open this file!!" << std::endl;
        return;
    }

}

bool FileManager::FindPlayer(const std::string &ID) {
    if (m_file.is_open()) {
        if(!this->Is_empty())
        {

            std::string line;
            std::string s = ID;
            m_file.seekp(SEEK_SET);
            while (!m_file.eof()) {
                getline(m_file, line);
                if (line.find(s) != std::string::npos) {
                    m_file.clear();
                    return true;
                }
            }
            m_file.clear();
            return false;
        }
        else
            return false;
    }
    else{
        std::cout << "Unable to open this file!!!" << std::endl;
        return true;
    }
}

FileManager::~FileManager() {
    m_file.clear();
    m_file.close();
    std::cout << "File closed \n";

}

std::string FileManager::GetPlayerHistory(const std::string &ID)  {
    if (m_file.is_open()) {
        if(!FindPlayer(ID)) {
            m_file.seekp(SEEK_SET);
            std::string line;
            std::string s = ID;
            while (!m_file.eof()) {
                getline(m_file, line);
                if (line.find(s) != std::string::npos) {
                    return line;
                }
            }
        }
        m_file.clear();
        return "This player does not exists! You need to add it first \n";

    }
    else
    {
        return "Unable to open this file \n";
    }


}

void FileManager::DeletePlayer(const Player &player) {
    std::string deleteline=GetPlayerHistory(player.GetId());
    std::string line;
    std::fstream aux;
    aux.open(tempFile ,std::fstream::in| std::fstream::out|std::fstream::app);
    m_file.seekg(0,std::ios::beg);
    while(getline(m_file,line)) {
        if (line != deleteline) {

            aux << line<<std::endl;
        }
    }
        aux.close();
        m_file.close();
        remove(fileName);
        rename(tempFile,fileName);



}
int FileManager::GetLastID() {
    if(m_file.is_open())
    {
        m_file.clear();
        m_file.seekg(-2, std::ios_base::end);
        if (!MoveToStartOfLine())
        { if(this->Is_empty())
                return 1;
        }
        else
            {
        std::string ID="";
        int id;
        m_file>>ID;
        ID.erase(0,7);
        std::stringstream s(ID);
        s>>id;
        return id;
        }
    }


}

bool FileManager::MoveToStartOfLine()
{
    m_file.seekg(-1, std::ios_base::cur);
    for(long i = m_file.tellg(); i > 0; i--)
    {
        if(m_file.peek() == '\n')
        {
            m_file.get();
            return true;
        }
        if(i <= 1) {
            m_file.seekg(0);
            return true;
        }
        m_file.seekg(i, std::ios_base::beg);
    }
    return false;
}

bool FileManager::Is_empty() {
    if(m_file.is_open())
    {
        m_file.clear();
        if(m_file.peek()==std::ifstream::traits_type::eof())
            return true;
        else
            return false;
    }
}


