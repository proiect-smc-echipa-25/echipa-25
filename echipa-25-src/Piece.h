//
// Created by Aglitoiu Marius on 19/11/2020.
//

#ifndef ECHIPA_25_PIECE_H
#define ECHIPA_25_PIECE_H

#include <iostream>
#include <vector>
#include "auxClasses/PieceShapes.h"
#include <SFML/Graphics/Color.hpp>

class Piece {
private:
    Piece(PieceShapes::Shapes shape, sf::Color color);

    std::pair<int, int> m_coords;
    std::vector<std::vector<int>> m_points;
    sf::Color m_color;

    std::vector<std::vector<int>> RandomShape();

    sf::Color RandomColor();

public:
    Piece();

    explicit Piece(PieceShapes::Shapes shape);

    Piece &operator=(const Piece &);

    ~Piece();

    void DefineShape(PieceShapes::Shapes shape);

    void MoveRight();

    void MoveLeft();

    void MoveDown();

    std::pair<int, int> GetCoords();

    std::vector<std::vector<int>> GetPoints();

    sf::Color GetColor();

    void Rotate();

    void Scale(int scaleVal);
    void DeleteDot(int row,int col);
};


#endif //ECHIPA_25_PIECE_H
