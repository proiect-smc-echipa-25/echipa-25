//
// Created by Aglitoiu Marius on 19/11/2020.
//

#ifndef ECHIPA_25_FILEMANAGER_H
#define ECHIPA_25_FILEMANAGER_H
#include <iostream>
#include <fstream>
#include<string>
#include "Player.h"



class FileManager {
public :
    FileManager();
    void AddPlayer( const Player& player);
    std::string GetPlayerHistory( const std::string& ID ) ;
    void DeletePlayer(const Player& player);
    int GetLastID();
    bool Is_empty();
    ~FileManager();

private:
    std::fstream m_file;
    bool FindPlayer(const std::string& ID);
    bool MoveToStartOfLine();


};


#endif //ECHIPA_25_FILEMANAGER_H
