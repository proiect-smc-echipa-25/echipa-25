//
// Created by Aglitoiu Marius on 19/11/2020.
//

#include "Player.h"
#include "Frontend.h"

void Player::SetScore(int score) {
    m_score = score;
}

void Player::SaveScore() {

}


Player::Player(const std::string &name)
        : m_playerName{name} {
    m_ID = "player_" + std::to_string(++nextID);
}

Player::Player()
        : m_playerName{"EMPTY"}, m_score{0}, m_maxScore{0}, m_ID{0}, m_gamesPlayed{0} {
    m_ID = "player_" + std::to_string(++nextID);
}

int Player::GetScore() const {
    return m_score;
}

const std::string &Player::GetPlayerName() const {
    return m_playerName;
}



int Player::GetGamesPlayed() const {
    return m_gamesPlayed;
}

int Player::GetMaxScore() const {
    return m_maxScore;
}

Player &Player::operator=(const Player & other) {
    m_playerName = other.m_playerName;
    m_gamesPlayed=other.m_gamesPlayed;
    m_ID=other.m_ID;
    m_maxScore=other.m_maxScore;
    m_score=other.m_score;
}

Player::Player(const Player & other) {
    *this = other;
}

Player::Player(Player && other) {
    *this = std::move(other);
}

Player::~Player()
{
    //std::cout<<"Desctructor called!\n";
}

void Player::SetNewMaxScore(int newScore) {
    if(newScore>=m_maxScore)
    {
        m_maxScore=newScore ;
    }
}

void Player::SetPlayerName(const std::string &mPlayerName) {
    m_playerName = mPlayerName;
}

//void Player::setId(int mId) {
  //  m_ID = mId;
//}

void Player::SetGamesPlayed(int mGamesPlayed) {
    m_gamesPlayed = mGamesPlayed;
}

void Player::SetMaxScore(int mMaxScore) {
    m_maxScore = mMaxScore;
}



const std::string &Player::GetId() const {
    return m_ID;
}
