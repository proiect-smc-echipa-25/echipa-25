//
// Created by Aglitoiu Marius on 19/11/2020.
//
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "auxClasses/PieceQueue.h"
#ifndef ECHIPA_25_BOARD_H
#define ECHIPA_25_BOARD_H

#include <vector>
#include <thread>
#include <iostream>
#include "Piece.h"

class Board : public sf::RenderTexture {
public:
    enum CollisionType {
        Left, Right, Bottom,BlackHole
    };

    void DecreaseSpeed();

    int GetSpeedLevel();

    void CheckAndClearLine(int row);

    bool GameOver();

    Board();

    Board(int lines, int columns);

    void Tick();

    void MoveRight();

    void MoveLeft();

    void DropDown();

    void Rotate();

    int GetScore();

private:
    int m_ticks;
    int m_speed_level;
    int m_dotSize;
    std::pair<unsigned ,unsigned >m_black_hole=std::pair<unsigned,unsigned>(0,0);
    std::pair<unsigned ,unsigned >m_star=std::pair<unsigned,unsigned>(0,0);
    Piece *m_fallingPiece;
    int m_score=0;
    std::vector<Piece> m_piecesVector;
    //std::thread speed();
    std::vector<std::vector<std::pair<int, sf::Color>>> m_boardMatrix;

    void DrawFallingPiece();
    void DrawBlackHole();
    void DrawBoard();
    void GenerateBlackHole();
    void DrawStar();
    void GenerateStar();


    bool Colliding(CollisionType colType);
    void SliceFallingPiece();
    void StickFallingPiece();

};


#endif //ECHIPA_25_BOARD_H
