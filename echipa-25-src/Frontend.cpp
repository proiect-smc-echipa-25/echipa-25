//
// Created by David on 2020. 12. 20..
//

#include "Frontend.h"
#include "Player.h"
#include "FileManager.h"

//
// Created by Aglitoiu Marius on 19/11/2020.
//

#include "Board.h"
#include<iostream>

const int xPosition = 200, yPosition = 300, buttonWidth = 190, buttonHeight = 50;
int nextID = 0;
int x_matrix, y_matrix;

bool IsMouseOver(sf::RenderWindow &window, sf::RectangleShape button) {
    float mouseX = sf::Mouse::getPosition(window).x;
    float mouseY = sf::Mouse::getPosition(window).y;

    float btnPosX = button.getPosition().x;
    float btnPosY = button.getPosition().y;

    float btnXPosWidth = button.getPosition().x + button.getLocalBounds().width;
    float btnYPosHeight = button.getPosition().y + button.getLocalBounds().height;

    return mouseX < btnXPosWidth && mouseX > btnPosX && mouseY < btnYPosHeight && mouseY > btnPosY;
}

void BindEvents(sf::RenderWindow &window, sf::Event event, Frontend::GameState &gameState, bool &WritePlayerName,
                bool &WritePlayerName2,
                std::string &Name,
                std::string &Name2,
                sf::Text &PlayerNameText,
                sf::Text &Player2NameText, Board *&firstBoard, Board *&secondBoard,
                std::vector<std::pair<sf::RectangleShape, sf::Text>> Buttons,
                sf::Text &ScoreNumber, Player &player, Player &player2, FileManager &File, bool &TwoPlayers) {
    bool multiplayer = false;
    switch (event.type) {

        case sf::Event::Closed:
            window.close();

        case sf::Event::MouseMoved:
            if (gameState == Frontend::GameState::StartMenu) {
                if (IsMouseOver(window, Buttons[2].first)) {
                    TwoPlayers = true;
                }
                if (IsMouseOver(window, Buttons[1].first)) {
                    TwoPlayers = false;
                }
            }
            break;

        case sf::Event::MouseButtonPressed:
            // NewGameSP
            if (IsMouseOver(window, Buttons[1].first) && gameState == Frontend::GameState::StartMenu) {
                gameState = Frontend::GameState::SizeOption;
                window.setSize(sf::Vector2u(600, 900));
                window.setView(sf::View(sf::FloatRect(0, 0, 600, 900)));
                break;
            }
            if (IsMouseOver(window, Buttons[11].first) && gameState == Frontend::GameState::SizeOption) {
                //small board
                x_matrix = 18;
                y_matrix = 5;
                firstBoard = new Board(x_matrix, y_matrix);
                gameState = Frontend::GameState::InGameSP;
                break;
            }
            if (IsMouseOver(window, Buttons[12].first) && gameState == Frontend::GameState::SizeOption) {
                //medium board
                x_matrix = 30;
                y_matrix = 10;
                firstBoard = new Board(x_matrix, y_matrix);
                gameState = Frontend::GameState::InGameSP;
                break;
            }
            if (IsMouseOver(window, Buttons[13].first) && gameState == Frontend::GameState::SizeOption) {
                //big board
                x_matrix = 45;
                y_matrix = 15;
                firstBoard = new Board(x_matrix, y_matrix);
                gameState = Frontend::GameState::InGameSP;
                break;
            }
            // MultiPlayerSelect
            if (IsMouseOver(window, Buttons[2].first) && gameState == Frontend::GameState::StartMenu) {
                gameState = Frontend::GameState::MultiPlayerOption;
                break;
            }

            //MultiPlayerVS
            if (IsMouseOver(window, Buttons[8].first) && gameState == Frontend::GameState::MultiPlayerOption) {
                gameState = Frontend::GameState::MultiPlayerVS;
                multiplayer = true;
                window.setSize(sf::Vector2u(900, 900));
                window.setView(sf::View(sf::FloatRect(0, 0, 900, 900)));
                break;
            }

            if (IsMouseOver(window, Buttons[9].first) && gameState == Frontend::GameState::MultiPlayerOption) {
                gameState = Frontend::GameState::MultiPlayerCoop;
                break;
            }
            // ResumeGame
            if (IsMouseOver(window, Buttons[5].first) && gameState == Frontend::GameState::Escape) {
                if (multiplayer)
                    gameState = Frontend::GameState::MultiPlayerVS;
                else
                    gameState = Frontend::GameState::InGameSP;
                break;
            }
            // RestartGame
            if (IsMouseOver(window, Buttons[3].first) && gameState == Frontend::GameState::Escape) {
                player.SetGamesPlayed(player.GetGamesPlayed() + 1);
                player.SetScore(firstBoard->GetScore());
                player.SetNewMaxScore(player.GetScore());
               // firstBoard = new Board(30, 10);
                if (multiplayer) {
                    gameState = Frontend::GameState::MultiPlayerVS;
                    firstBoard = new Board(x_matrix, y_matrix);
                    secondBoard = new Board(x_matrix, y_matrix);
                } else {
                    gameState = Frontend::GameState::InGameSP;
                    firstBoard = new Board(x_matrix, y_matrix);
                }

                break;
            }
            if (IsMouseOver(window, Buttons[0].first)) {
                WritePlayerName = true;
            } else {
                WritePlayerName = false;
            }
            if (IsMouseOver(window, Buttons[7].first)) {
                WritePlayerName2 = true;
            } else {
                WritePlayerName2 = false;
            }
            //Decrease Speed
            if (IsMouseOver(window, Buttons[6].first)) {
                if (firstBoard->GetSpeedLevel() > 1) {
                    firstBoard->DecreaseSpeed();
                }
                if (secondBoard->GetSpeedLevel() > 1) {
                    secondBoard->DecreaseSpeed();
                }
            }
            //ExitGame
            if (IsMouseOver(window, Buttons[4].first)) {
                if (multiplayer) {
                    player.SetPlayerName(Name);
                    player2.SetPlayerName(Name2);
                    player.SetScore(firstBoard->GetScore());
                    player2.SetScore(secondBoard->GetScore());
                    player.SetGamesPlayed(player.GetGamesPlayed() + 1);
                    player2.SetGamesPlayed(player2.GetGamesPlayed() + 1);
                    player.SetNewMaxScore(player.GetScore());
                    player2.SetNewMaxScore(player2.GetScore());
                    File.AddPlayer(player);
                    File.AddPlayer(player2);
                } else {
                    player.SetPlayerName(Name);
                    player.SetScore(firstBoard->GetScore());
                    player.SetGamesPlayed(player.GetGamesPlayed() + 1);
                    player.SetNewMaxScore(player.GetScore());
                    File.AddPlayer(player);

                }
                window.close();
            }
            break;
        case sf::Event::TextEntered:
            if (gameState == Frontend::GameState::StartMenu) {
                // if backspace key is pressed
                if (WritePlayerName && event.text.unicode == 8 && Name.size()) {
                    Name.pop_back();
                    PlayerNameText.setString(Name);
                }
                if (Name.size() < 17) {
                    if (WritePlayerName && event.text.unicode != 8 && event.text.unicode != 13 &&
                        event.text.unicode < 128) {
                        Name += (char) event.text.unicode;
                        PlayerNameText.setString(Name);
                    }
                }
                if (WritePlayerName2 && event.text.unicode == 8 && Name2.size()) {
                    Name2.pop_back();
                    Player2NameText.setString(Name2);
                }
                if (Name2.size() < 17) {
                    if (WritePlayerName2 && event.text.unicode != 8 && event.text.unicode != 13 &&
                        event.text.unicode < 128) {
                        Name2 += (char) event.text.unicode;
                        Player2NameText.setString(Name2);
                    }
                }
            }
            break;
        case sf::Event::KeyPressed:
            // if esc key is pressed
            if (event.key.code == sf::Keyboard::Escape) {
                gameState = Frontend::GameState::Escape;
            }
            if (gameState == Frontend::GameState::InGameSP) {
                switch (event.key.code) {
                    case sf::Keyboard::Up:
                        firstBoard->Rotate();
                        break;
                    case sf::Keyboard::Left:
                        firstBoard->MoveLeft();
                        break;
                    case sf::Keyboard::Right:
                        firstBoard->MoveRight();
                        break;
                    case sf::Keyboard::Down:
                        firstBoard->DropDown();
                        break;
                }
            } else if (gameState == Frontend::GameState::MultiPlayerVS) {
                switch (event.key.code) {
                    case sf::Keyboard::W:
                        firstBoard->Rotate();
                        break;
                    case sf::Keyboard::A:
                        firstBoard->MoveLeft();
                        break;
                    case sf::Keyboard::D:
                        firstBoard->MoveRight();
                        break;
                    case sf::Keyboard::S:
                        firstBoard->DropDown();
                        break;
                    case sf::Keyboard::Up:
                        secondBoard->Rotate();
                        break;
                    case sf::Keyboard::Left:
                        secondBoard->MoveLeft();
                        break;
                    case sf::Keyboard::Right:
                        secondBoard->MoveRight();
                        break;
                    case sf::Keyboard::Down:
                        secondBoard->DropDown();
                        break;
                }
            } else if (gameState == Frontend::GameState::MultiPlayerCoop) {
                switch (event.key.code) {
                    case sf::Keyboard::W:
                        firstBoard->Rotate();
                        break;
                    case sf::Keyboard::A:
                        firstBoard->MoveLeft();
                        break;
                    case sf::Keyboard::D:
                        firstBoard->MoveRight();
                        break;
                    case sf::Keyboard::S:
                        firstBoard->DropDown();
                        break;
                    case sf::Keyboard::Up:
                        firstBoard->Rotate();
                        break;
                    case sf::Keyboard::Left:
                        firstBoard->MoveLeft();
                        break;
                    case sf::Keyboard::Right:
                        firstBoard->MoveRight();
                        break;
                    case sf::Keyboard::Down:
                        firstBoard->DropDown();
                        break;
                }
            }
            break;
    }
}

sf::RectangleShape
Rectangle(int Width, int Height, int xPos, int yPos, sf::Color Color, sf::Color OutlineColor, int thickness) {
    sf::RectangleShape NewButton(sf::Vector2f(Width, Height));
    NewButton.setPosition(xPos, yPos);
    NewButton.setFillColor(Color);
    NewButton.setOutlineColor(OutlineColor);
    NewButton.setOutlineThickness(thickness);
    return NewButton;
}

sf::Text
SetText(sf::Color Color, const std::string &string, int CharacterSize, int xPos, int yPos, const sf::Font &font) {
    sf::Text text;
    text.setString(string);
    text.setFillColor(Color);
    text.setCharacterSize(CharacterSize);
    text.setPosition(xPos, yPos);
    text.setFont(font);
    return text;
}

void SetPositionText(sf::Text &text, int xPos, int yPos) {
    text.setPosition(xPos, yPos);
}

void SetCharacterSizeText(sf::Text &text, const int &CharSize) {
    text.setCharacterSize(CharSize);
}

void Frontend::Game(FileManager &File) {
    window.create(sf::VideoMode(600, 900), "Tetris! Team_25");
    if (!font.loadFromFile("FontFile.ttf")) {
        std::cout << "Error loading FontFile" << std::endl;
    }
    InitButtons();
    auto *firstBoard = new Board(30, 10);
    auto *secondBoard = new Board(30, 10);
    sf::Text PlayerNameText = SetText(sf::Color::White, "", 20, xPosition - 9, yPosition - 63, font);
    sf::Text Player2NameText = SetText(sf::Color::White, "", 20, xPosition - 9, yPosition - 63, font);
    sf::Text ScoreText = SetText(sf::Color::White, "Score :", 40, xPosition, yPosition, font);
    sf::Text LeftScoreText = SetText(sf::Color::White, "Left :", 40, xPosition, yPosition, font);
    sf::Text RightScoreText = SetText(sf::Color::White, "Right :", 40, xPosition, yPosition, font);
    sf::Text ScoreNumber = SetText(sf::Color::White, std::to_string(firstBoard->GetScore()), 40, xPosition, yPosition,
                                   font);
    sf::Text MultiPlayerOptionText = SetText(sf::Color::White, "Choose your gamemode!", 40, xPosition - 85,
                                             yPosition - 150, font);
    sf::Text SizeChoiceText = SetText(sf::Color::White, "Choose the size of the table!", 40, xPosition - 125,
                                      yPosition - 150, font);
    sf::Text SecondScoreNumber = SetText(sf::Color::White, std::to_string(secondBoard->GetScore()), 40, xPosition,
                                         yPosition,
                                         font);
    PieceQueue::InitQueue();

    bool WritePlayerName = false;
    bool WritePlayerName2 = false;
    std::string Name;
    std::string Name2;
    if (File.Is_empty()) {
        nextID = 0;
    } else {
        nextID = File.GetLastID();
    }
    Player player;
    Player player2;
    int ticks = 0;
    sf::Texture pieceQueueTexture;
    bool TwoPlayers = false;
    while (window.isOpen()) {
        sf::Event event;
        while (window.pollEvent(event)) {
            BindEvents(window, event, gameState, WritePlayerName, WritePlayerName2, Name, Name2, PlayerNameText,
                       Player2NameText, firstBoard, secondBoard,
                       Buttons, ScoreNumber, player, player2, File, TwoPlayers);
        }
        window.clear();
        switch (gameState) {
            case Frontend::GameState::StartMenu: {

                window.draw(Buttons[0].second); // enterNameText
                window.draw(Buttons[0].first); // playerNameInput
                window.draw(PlayerNameText); // playerNameText
                window.draw(Buttons[1].first); // newGameSPBtn
                window.draw(Buttons[1].second); // newGameSPText
                window.draw(Buttons[2].first); // newGameMPBtn
                window.draw(Buttons[2].second); // newGameMPText
                Buttons[4].first.setPosition(xPosition, yPosition + 200);
                Buttons[4].second.setPosition(xPosition + 40, yPosition + 205);
                window.draw(Buttons[4].first); // exitGameBtn
                window.draw(Buttons[4].second); // exitGameText
                if (TwoPlayers) {
                    PlayerNameText.setPosition(xPosition - 95, yPosition - 63);
                    Player2NameText.setPosition(xPosition + 135, yPosition - 63);
                    Player2NameText.setCharacterSize(20);
                    Buttons[0].second.setString("Enter player 1 name");
                    Buttons[0].first.setPosition(xPosition - 100, yPosition - 65);
                    Buttons[0].second.setPosition(xPosition - 108, yPosition - 110);
                    Buttons[7].first.setPosition(xPosition + 130, yPosition - 65);
                    Buttons[7].second.setPosition(xPosition + 120, yPosition - 110);
                    window.draw(Buttons[7].first);
                    window.draw(Buttons[7].second);
                    window.draw(Player2NameText); // player2NameText
                } else {
                    PlayerNameText.setPosition(xPosition + 6, yPosition - 63);
                    Player2NameText.setPosition(xPosition + 120, yPosition - 10000);
                    Buttons[7].first.setPosition(xPosition, yPosition - 10000);
                    Buttons[7].second.setPosition(xPosition + 10, yPosition - 10000);
                    Buttons[0].second.setString("Enter your name");
                    Buttons[0].first.setPosition(xPosition, yPosition - 65);
                    Buttons[0].second.setPosition(xPosition + 10, yPosition - 110);
                }
                break;
            }
            case Frontend::GameState::InGameSP : {
                const sf::Texture &boardTexture = firstBoard->getTexture();
                sf::Sprite boardSprite(boardTexture);

                if (firstBoard->GameOver()) {
                    // GAME OVER
                    player.SetPlayerName(Name);
                    player.SetScore(firstBoard->GetScore());
                    player.SetGamesPlayed(player.GetGamesPlayed() + 1);
                    player.SetNewMaxScore(player.GetScore());
                    File.AddPlayer(player);
                    Name.clear();
                    firstBoard = new Board(sizeX, sizeY);
                    PlayerNameText.setString(Name);
                    gameState = Frontend::GameState::StartMenu;
                } else {
                    firstBoard->Tick();
                    if (ticks % 500 == 0) {
                        pieceQueueTexture = RenderPieceQueue();
                    }
                }
                window.draw(boardSprite);
                SetCharacterSizeText(PlayerNameText, 40);
                SetPositionText(PlayerNameText, xPosition + 140, yPosition - 190);
                SetPositionText(ScoreText, xPosition + 140, yPosition - 130);
                SetPositionText(ScoreNumber, xPosition + 255, yPosition - 130);
                ScoreNumber.setString(std::to_string(firstBoard->GetScore()));
                window.draw(PlayerNameText);
                window.draw(ScoreText);
                window.draw(ScoreNumber);
                Buttons[6].first.setPosition(xPosition + 140, yPosition - 235);
                Buttons[6].second.setPosition(xPosition + 147, yPosition - 237);
                window.draw(Buttons[6].first); // DecreaseSpeedGameBtn
                window.draw(Buttons[6].second); // DecreaseSpeedGameText
                sf::Sprite pQueueSprite(pieceQueueTexture);
                pQueueSprite.setPosition(350, 250);
                window.draw(pQueueSprite);
                ticks++;
                break;

            }
            case Frontend::GameState::MultiPlayerOption: {
                window.draw(MultiPlayerOptionText);
                window.draw(Buttons[8].first);
                window.draw(Buttons[8].second);
                window.draw(Buttons[9].first);
                window.draw(Buttons[9].second);
                break;
            }
            case ::Frontend::GameState::SizeOption: {
                window.draw(SizeChoiceText);
                window.draw(Buttons[11].first);
                window.draw(Buttons[11].second);
                window.draw(Buttons[12].first);
                window.draw(Buttons[12].second);
                window.draw(Buttons[13].first);
                window.draw(Buttons[13].second);
                break;
            }
            case Frontend::GameState::MultiPlayerCoop: {
                const sf::Texture &boardTexture = firstBoard->getTexture();
                sf::Sprite boardSprite(boardTexture);

                if (firstBoard->GameOver()) {
                    // GAME OVER
                    player.SetPlayerName(Name);
                    player.SetScore(firstBoard->GetScore());
                    player.SetGamesPlayed(player.GetGamesPlayed() + 1);
                    File.AddPlayer(player);
                    Name.clear();
                    firstBoard = new Board(sizeX, sizeY);
                    PlayerNameText.setString(Name);
                    gameState = Frontend::GameState::StartMenu;
                } else {
                    firstBoard->Tick();
                    if (ticks % 500 == 0) {
                        pieceQueueTexture = RenderPieceQueue();
                    }
                }
            }
            case Frontend::GameState::MultiPlayerVS: {
                const sf::Texture &board1Texture = firstBoard->getTexture();
                const sf::Texture &board2Texture = secondBoard->getTexture();
                sf::Sprite board1Sprite(board1Texture);
                board1Sprite.setPosition(0, 0);
                sf::Sprite board2Sprite(board2Texture);
                board2Sprite.setPosition(600, 0);
                if (firstBoard->GameOver()) {
                    // GAME OVER
                    player.SetPlayerName(Name);
                    player2.SetPlayerName(Name2);
                    player.SetScore(firstBoard->GetScore());
                    player2.SetScore(secondBoard->GetScore());
                    player.SetGamesPlayed(player.GetGamesPlayed() + 1);
                    player2.SetGamesPlayed(player2.GetGamesPlayed() + 1);
                    player.SetNewMaxScore(player.GetScore());
                    player2.SetNewMaxScore(player.GetScore());
                    File.AddPlayer(player);
                    File.AddPlayer(player2);
                    Name.clear();
                    Name2.clear();
                    firstBoard = new Board(30, 20);
                    PlayerNameText.setString(Name);
                    Player2NameText.setString(Name2);
                    gameState = Frontend::GameState::StartMenu;
                } else {
                    firstBoard->Tick();
                    secondBoard->Tick();
                    if (ticks % 500 == 0) {
                        pieceQueueTexture = RenderPieceQueue();
                    }
                }
                sf::Sprite pQueueSprite(pieceQueueTexture);
                pQueueSprite.setPosition(400, 250);

                window.draw(board1Sprite);
                window.draw(board2Sprite);
                SetPositionText(LeftScoreText, xPosition + 170, yPosition - 130);
                SetPositionText(ScoreNumber, xPosition + 280, yPosition - 130);
                SetPositionText(RightScoreText, xPosition + 170, yPosition + 450);
                SetPositionText(SecondScoreNumber, xPosition + 280, yPosition + 450);
                ScoreNumber.setString(std::to_string(firstBoard->GetScore()));
                SecondScoreNumber.setString(std::to_string(secondBoard->GetScore()));

                window.draw(LeftScoreText);
                window.draw(RightScoreText);
                window.draw(ScoreNumber);
                window.draw(SecondScoreNumber);
                window.draw(pQueueSprite);
                Buttons[6].first.setPosition(xPosition + 140, yPosition - 235);
                Buttons[6].second.setPosition(xPosition + 147, yPosition - 237);
                window.draw(Buttons[6].first); // DecreaseSpeedGameBtn
                window.draw(Buttons[6].second); // DecreaseSpeedGameText
                break;
            }
            case Frontend::GameState::Escape: {
                window.draw(Buttons[5].first); // resumeGameBtn
                window.draw(Buttons[5].second); // resumeGameText
                window.draw(Buttons[3].first); // restartGameBtn
                window.draw(Buttons[3].second); // restartGameText
                Buttons[4].first.setPosition(xPosition, yPosition + 205);
                Buttons[4].second.setPosition(xPosition + 36, yPosition + 210);
                window.draw(Buttons[4].first); // exitGameBtn
                window.draw(Buttons[4].second); // exitGameText
                break;
            }

        }

        window.display();
    }
}

sf::Texture Frontend::RenderPieceQueue() {
    sf::RenderTexture rt;
    rt.clear();
    std::vector<Piece> queueIteration = PieceQueue::GetIteration();
    if (!rt.create(200, 450)) {
        std::cerr << "err";
    }
    rt.clear();
    int dotSize = 30;
    int x = 0, y = 0;
    for (int index = 0; index < 3; index++) {
        Piece qPiece = queueIteration.at(index);
        for (std::vector<int> const &row:qPiece.GetPoints()) {
            for (int cell:row) {
                sf::RectangleShape bgDot;
                bgDot.setOutlineThickness(1);
                bgDot.setOutlineColor(sf::Color::White);
                bgDot.setPosition(x, y);
                bgDot.setSize(sf::Vector2f(dotSize, dotSize));
                if (cell == 1) {
                    bgDot.setFillColor(qPiece.GetColor());
                    rt.draw(bgDot);
                } else {
                    bgDot.setFillColor(sf::Color::Black);
                    bgDot.setOutlineColor(sf::Color::Black);
                    rt.draw(bgDot);
                }
                x += dotSize;
            }
            x = 0;
            y += dotSize;
        }
        y += dotSize;
    }

    return rt.getTexture();
}

void Frontend::InitButtons() {
    sf::Text EnterName = SetText(sf::Color::White, "Enter your name", 25, xPosition + 10, yPosition - 110, font);
    sf::RectangleShape PlayerNameInput = Rectangle(buttonWidth, buttonHeight - 20, xPosition, yPosition - 65,
                                                   sf::Color::Black, sf::Color::White, 3);
    Buttons.push_back(std::make_pair(PlayerNameInput, EnterName));


    sf::Text NewGameSPText = SetText(sf::Color::White, "SinglePlayer", 30, xPosition + 15, yPosition + 5, font);
    sf::RectangleShape NewGameSPButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition, sf::Color::Black,
                                                   sf::Color::White, 5);

    Buttons.push_back(std::make_pair(NewGameSPButton, NewGameSPText));

    sf::Text NewGameMPText = SetText(sf::Color::White, "MultiPlayer", 30, xPosition + 20, yPosition + 105, font);
    sf::RectangleShape NewGameMPButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition + 100,
                                                   sf::Color::Black,
                                                   sf::Color::White, 5);

    Buttons.push_back(std::make_pair(NewGameMPButton, NewGameMPText));

    sf::Text RestartGameText = SetText(sf::Color::White, "Restart", 30, xPosition + 45, yPosition + 110, font);
    sf::RectangleShape RestartGameButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition + 105,
                                                     sf::Color::Black, sf::Color::White, 5);
    Buttons.push_back(std::make_pair(RestartGameButton, RestartGameText));

    sf::Text ExitGameText = SetText(sf::Color::White, "Exit Game", 30, xPosition + 40, yPosition + 305, font);
    sf::RectangleShape ExitGameButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition + 300,
                                                  sf::Color::Black, sf::Color::White, 5);
    Buttons.push_back(std::make_pair(ExitGameButton, ExitGameText));

    sf::Text ResumeGameText = SetText(sf::Color::White, "Resume", 30, xPosition + 48, yPosition + 5, font);
    sf::RectangleShape ResumeGameButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition, sf::Color::Black,
                                                    sf::Color::White, 5);
    Buttons.push_back(std::make_pair(ResumeGameButton, ResumeGameText));

    sf::Text DecreaseSpeedGameText = SetText(sf::Color::White, "Speed --", 30, xPosition, yPosition, font);
    sf::RectangleShape DecreaseSpeedGameButton = Rectangle(buttonWidth - 80, buttonHeight - 15, xPosition, yPosition,
                                                           sf::Color::Black,
                                                           sf::Color::White, 3);
    Buttons.push_back(std::make_pair(DecreaseSpeedGameButton, DecreaseSpeedGameText));

    sf::Text EnterName2 = SetText(sf::Color::White, "Enter player 2 name", 25, xPosition + 40, yPosition + 405, font);
    sf::RectangleShape Player2NameInput = Rectangle(buttonWidth, buttonHeight - 20, xPosition, yPosition + 400,
                                                    sf::Color::Black, sf::Color::White, 3);
    Buttons.push_back(std::make_pair(Player2NameInput, EnterName2));

    sf::Text NewGameMPCoopText = SetText(sf::Color::White, "VS", 30, xPosition + 80, yPosition, font);
    sf::RectangleShape NewGameMPCoopButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition - 5,
                                                       sf::Color::Black, sf::Color::White, 5);
    Buttons.push_back(std::make_pair(NewGameMPCoopButton, NewGameMPCoopText));

    sf::Text NewGameMPVSText = SetText(sf::Color::White, "Co-op", 30, xPosition + 60, yPosition + 150, font);
    sf::RectangleShape NewGameMPVSButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition + 145,
                                                     sf::Color::Black, sf::Color::White, 5);
    Buttons.push_back(std::make_pair(NewGameMPVSButton, NewGameMPVSText));

    sf::Text MainMenuText = SetText(sf::Color::White, "Main Menu", 30, xPosition + 40, yPosition + 153, font);
    sf::RectangleShape MainMenuButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition + 150,
                                                  sf::Color::Black, sf::Color::White, 5);
    Buttons.push_back(std::make_pair(MainMenuButton, MainMenuText));

    sf::Text SmallChoiceText = SetText(sf::Color::White, "5X18", 30, xPosition + 67, yPosition, font);
    sf::RectangleShape SmallChoiceButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition - 5,
                                                     sf::Color::Black, sf::Color::White, 5);
    Buttons.push_back(std::make_pair(SmallChoiceButton, SmallChoiceText));

    sf::Text MediumChoiceText = SetText(sf::Color::White, "10X30", 30, xPosition + 66, yPosition + 100, font);
    sf::RectangleShape MediumChoiceButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition + 95,
                                                      sf::Color::Black, sf::Color::White, 5);
    Buttons.push_back(std::make_pair(MediumChoiceButton, MediumChoiceText));

    sf::Text BigChoiceText = SetText(sf::Color::White, "15x45", 30, xPosition + 66, yPosition + 200, font);
    sf::RectangleShape BigChoiceButton = Rectangle(buttonWidth, buttonHeight, xPosition, yPosition + 195,
                                                   sf::Color::Black, sf::Color::White, 5);
    Buttons.push_back(std::make_pair(BigChoiceButton, BigChoiceText));
// to not use these and use the ones from vector
}

void Frontend::StartMenu() {
    window.draw(Buttons[0].second); // enterNameText
    window.draw(Buttons[0].first); // playerNameInput
    //window.draw(PlayerNameText); // playerNameText
    window.draw(Buttons[1].first); // newGameSPBtn
    window.draw(Buttons[1].second); // newGameSPText
    window.draw(Buttons[2].first); // newGameMPBtn
    window.draw(Buttons[2].second); // newGameMPText
    window.draw(Buttons[3].first); // restartGameBtn
    window.draw(Buttons[3].second); // restartGameText
    window.draw(Buttons[4].first); // exitGameBtn
    window.draw(Buttons[4].second); // exitGameText
    window.draw(Buttons[7].first);//enterName2Text
    window.draw(Buttons[7].second);//player2NameInput

}




