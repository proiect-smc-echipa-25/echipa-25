//
// Created by Aglitoiu Marius on 19/11/2020.
//

#include "Piece.h"
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

Piece::Piece() {
// piece without pre-known shape
    m_coords = std::pair<int, int>(0, 0);
    m_color = RandomColor();
    m_points = RandomShape();
}

Piece::Piece(PieceShapes::Shapes shape) {
    // piece with pre-known shape
    m_points = PieceShapes::getShape(shape);
    m_color = RandomColor();
}

Piece::Piece(PieceShapes::Shapes shape, sf::Color color) {
    // piece with pre-known shape
    m_points = PieceShapes::getShape(shape);
    this->m_color = color;
}

void Piece::DefineShape(PieceShapes::Shapes shape) {
    m_points = PieceShapes::getShape(shape);
}

void Piece::MoveRight() {
    m_coords.first += 1;
}

void Piece::MoveLeft() {
    m_coords.first -= 1;
}

void Piece::MoveDown() {
    m_coords.second += 1;
}

void Piece::Rotate() {
    int size = m_points.size();
    for (int x = 0; x < size / 2; x++) {
        for (int y = x; y < size - x - 1; y++) {
            int temp = m_points.at(x).at(y);
            m_points.at(x).at(y) = m_points.at(y).at(size - 1 - x);
            m_points.at(y).at(size - 1 - x)
                    = m_points.at(size - 1 - x).at(size - 1 - y);
            m_points.at(size - 1 - x).at(size - 1 - y)
                    = m_points.at(size - 1 - y).at(x);
            m_points.at(size - 1 - y).at(x) = temp;
        }
    }
}


std::pair<int, int> Piece::GetCoords() {
    return m_coords;
// don't know yet if needed
}

void Piece::Scale(int scaleVal) {

}

std::vector<std::vector<int>> Piece::GetPoints() {
    return m_points;

}

Piece::~Piece() {

}

Piece &Piece::operator=(const Piece &other) {
    m_coords = other.m_coords;
    m_points = other.m_points;
}

sf::Color Piece::RandomColor() {
    int randomColorIndex = rand() % 6;
    switch (randomColorIndex) {
        case 0:
            return sf::Color::Red;
        case 1:
            return sf::Color::Cyan;
        case 2:
            return sf::Color::Blue;
        case 3:
            return sf::Color::Green;
        case 4:
            return sf::Color::Yellow;
        case 5:
            return sf::Color::Magenta;
    }
    return sf::Color();
}

std::vector<std::vector<int>> Piece::RandomShape() {
    int randomIndex = rand() % 6;
    return PieceShapes::getShape(randomIndex);
}

sf::Color Piece::GetColor() {
    return m_color;
}

void Piece::DeleteDot(int row, int col) {
    m_points.at(row).at(col) = 0;
}

