//
// Created by Aglitoiu Marius on 19/11/2020.
//

#ifndef ECHIPA_25_PLAYER_H
#define ECHIPA_25_PLAYER_H

#include<iostream>
#include<string>


class Player{

public:

    Player(const std::string&);
    Player();
    Player(const Player&);
    Player(Player&&);
    Player& operator = (const Player&);
    ~Player();
    void SetScore(int score);
    void SaveScore();
    const std::string& GetId() const;
    int GetGamesPlayed() const;
    int GetMaxScore() const;
    int GetScore() const;
    const std::string &GetPlayerName() const;
    void SetNewMaxScore(int newScore);



public:
    void SetPlayerName(const std::string &mPlayerName);

    void SetGamesPlayed(int mGamesPlayed);

    void SetMaxScore(int mMaxScore);



private:
    std::string m_playerName;
    std::string m_ID;
    int m_gamesPlayed;
    int m_maxScore;
    int m_score;
};




#endif //ECHIPA_25_PLAYER_H
