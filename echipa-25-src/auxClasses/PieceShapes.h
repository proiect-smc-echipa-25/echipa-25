//
// Created by Aglitoiu Marius on 29/11/2020.
//

#ifndef ECHIPA_25_PIECESHAPES_H
#define ECHIPA_25_PIECESHAPES_H

#include <iostream>
#include <vector>
#include <random>

class PieceShapes {
private:
    static const std::vector<std::vector<std::vector<int>>> shapes;
public:
    enum Shapes {
        O, S, Z, T, L, J, I
    };

    static std::vector<std::vector<int>> getShape(Shapes choice) {
        switch (choice) {
            case O:
                return shapes[0];
            case S:
                return shapes[1];
            case Z:
                return shapes[2];
            case T:
                return shapes[3];
            case L:
                return shapes[4];
            case J:
                return shapes[5];
            case I:
                return shapes[6];
        }
    }

    static std::vector<std::vector<int>> getShape(int index) {
        if (index > shapes.size() || index < 0) {
            return shapes[0];
        }
        return shapes[index];
    }
};
#endif //ECHIPA_25_PIECESHAPES_H
