//
// Created by Aglitoiu Marius on 09/12/2020.
//
#include <queue>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "../Piece.h"

#ifndef ECHIPA_25_PIECEQUEUE_H
#define ECHIPA_25_PIECEQUEUE_H


class PieceQueue {
private:
    static std::queue<Piece> m_queue;

public:
    static void InitQueue();

    static Piece Extract();

    static std::vector<Piece> GetIteration();

};


#endif //ECHIPA_25_PIECEQUEUE_H
