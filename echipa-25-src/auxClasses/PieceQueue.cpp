//
// Created by Aglitoiu Marius on 09/12/2020.
//

#include "PieceQueue.h"
std::queue<Piece> PieceQueue::m_queue;
void PieceQueue::InitQueue() {
    for (int index = 0; index < 3; index++) {
        Piece *piece = new Piece();
        m_queue.push(*piece);
    }
}

Piece PieceQueue::Extract() {
    Piece returnPiece = m_queue.front();
    m_queue.pop();
    auto *piece = new Piece();
    m_queue.push(*piece);
    return returnPiece;
}

std::vector<Piece> PieceQueue::GetIteration() {
    std::queue<Piece> cloneQueue = m_queue;
    std::vector<Piece> pieces;
    if (m_queue.size() >= 3) {
        for (int index = 0; index < 3; index++) {
            pieces.push_back(cloneQueue.front());
            cloneQueue.pop();
        }
    }
    return pieces;

}
