//
// Created by Aglitoiu Marius on 19/11/2020.
//

#include "Board.h"

Board::Board() {
    m_ticks = 0;
    m_speed_level = 0;
}

Board::Board(int lines, int columns) {
    m_ticks = 0;
    this->m_boardMatrix.resize(lines);
    for (auto &index : this->m_boardMatrix)
        index.resize(columns);
    m_fallingPiece = new Piece();
    m_speed_level = 0;
    DrawBoard();
}


void Board::CheckAndClearLine(int row) {
    bool fullLine = true;
    for (std::pair<int, sf::Color> cell:m_boardMatrix[row]) {
        if (cell.first == 0) {
            fullLine = false;
        }
    }
    if (fullLine) {
        for (int rowIndex = row; rowIndex > 0; rowIndex--) {
            m_boardMatrix[rowIndex] = m_boardMatrix[rowIndex - 1];
        }
        m_score += m_boardMatrix.size() / 3;
    }

}

int Board::GetSpeedLevel() {
    return m_speed_level;
}

bool Board::GameOver() {
    for (std::pair<int, sf::Color> cell:m_boardMatrix[2]) {
        if (cell.first == 1) {
            return true;
        }
    }
    return false;
}

void Board::DrawBoard() {
    if (!create(600, 900)) {
        std::cout << "err";
        // error...
    }
    clear();
    m_dotSize = 900 / m_boardMatrix.size();
    int x = 1, y = 0;
    for (std::vector<std::pair<int, sf::Color>> row:m_boardMatrix) {
        for (std::pair<int, sf::Color> cell:row) {
            sf::RectangleShape bgDot;
            bgDot.setOutlineThickness(1);
            bgDot.setOutlineColor(sf::Color::White);
            bgDot.setPosition(x, y);
            bgDot.setSize(sf::Vector2f(m_dotSize, m_dotSize));
            display();
            if (cell.first == 0) {
                bgDot.setFillColor(sf::Color::Black);
            } else {
                bgDot.setFillColor(cell.second);
            }
            draw(bgDot);
            x += m_dotSize;
        }
        x = 1;
        y += m_dotSize;
    }
}

void Board::DecreaseSpeed() {
    m_speed_level--;
}

void Board::Tick() {
    if (m_ticks % 10000==0) {
        m_speed_level+=1;
    }
    if (m_ticks % 400 == 0) {
        if (Colliding(Board::CollisionType::Bottom)) {
            StickFallingPiece();
            m_fallingPiece = new Piece(PieceQueue::Extract());
        } else {
            if (Colliding(Board::CollisionType::BlackHole)) {
                SliceFallingPiece();
            }
            int falling = m_speed_level;
            std::cout << falling << std::endl;
            while (falling) {
                if (!Colliding(Board::CollisionType::Bottom)) {
                    m_fallingPiece->MoveDown();
                }
                falling--;
            }
        }
    }
    if (m_ticks && m_ticks % 10000 == 0) {
        GenerateStar();
    }
    if (m_ticks && m_ticks % 10000 == 0) {
        GenerateBlackHole();
    }
    if (!GameOver()) {
        m_ticks++;
        DrawBoard();
        DrawFallingPiece();
        DrawBlackHole();
        DrawStar();
    }


}

void Board::DrawFallingPiece() {
    int x = m_fallingPiece->GetCoords().first * m_dotSize, y = m_fallingPiece->GetCoords().second * m_dotSize;
    for (std::vector<int> row:m_fallingPiece->GetPoints()) {
        bool emptyLine = true;
        for (int cell:row) {
            if (cell == 1) {
                emptyLine = false;
                sf::RectangleShape bgDot;
                bgDot.setFillColor(m_fallingPiece->GetColor());
                bgDot.setOutlineThickness(1);
                bgDot.setOutlineColor(sf::Color::White);
                bgDot.setPosition(x, y);
                bgDot.setSize(sf::Vector2f(m_dotSize, m_dotSize));
                draw(bgDot);
                display();
            }
            x += m_dotSize;
        }
        x = m_fallingPiece->GetCoords().first * m_dotSize;
        if (!emptyLine) {
            y += m_dotSize;
        }
    }
}

void Board::GenerateBlackHole() {
    int x = rand() % m_boardMatrix.at(0).size();
    int y = rand() % m_boardMatrix.size() - 8;
    m_black_hole = std::pair<unsigned, unsigned>(x, y);
}

void Board::GenerateStar() {
    for (int index = m_boardMatrix.size() - 1; index >= 0; index--) {
        for (int index2 = m_boardMatrix.at(index).size() - 1; index2 >= 0; index2--) {
            if (m_boardMatrix[index][index2] == std::pair<int, sf::Color>(0, sf::Color::Black)) {
                m_star = std::pair<unsigned, unsigned>(index, index2);
                return;
            }
        }
    }
}

void Board::DrawStar() {
    if (m_star.first > 0) {
        m_boardMatrix.at(m_star.first).at(m_star.second) = std::pair<int, sf::Color>(1, sf::Color::White);
    }
}

void Board::DrawBlackHole() {
    int x = m_black_hole.first;
    int y = m_black_hole.second;
    if (x != 0 && y != 0) {
        sf::RectangleShape bgDot;
        bgDot.setFillColor(sf::Color::White);
        bgDot.setOutlineThickness(1);
        bgDot.setOutlineColor(sf::Color::White);
        bgDot.setPosition(x * m_dotSize, y * m_dotSize);
        bgDot.setSize(sf::Vector2f(m_dotSize, m_dotSize));
        draw(bgDot);
        display();
    }
}

bool Board::Colliding(Board::CollisionType colType) {
    std::vector<std::vector<int>> piecePoints = m_fallingPiece->GetPoints();
    std::pair<int, int> pieceCoords = m_fallingPiece->GetCoords();
    for (int row = 0; row < piecePoints.size(); row++) {
        for (int col = 0; col < piecePoints[0].size(); col++) {
            int matrixRow = pieceCoords.second + row;
            int matrixCol = pieceCoords.first + col;
            if (colType == Board::CollisionType::Bottom) {
                if (piecePoints.at(row).at(col) == 1) {
                    if (matrixRow == m_boardMatrix.size() - 1) {
                        return true;
                    }
                    if (m_boardMatrix.at(matrixRow + 1).at(matrixCol).first == 1) {
                        return true;
                    }
                }
                if (matrixRow == m_boardMatrix.size()) {
                    return true;
                }
            } else if (colType == Board::CollisionType::Left) {
                if (piecePoints.at(row).at(col) == 1) {
                    if (matrixCol == 0) {
                        return true;
                    }
                    if (m_boardMatrix.at(matrixRow).at(matrixCol - 1).first == 1) {
                        return true;
                    }
                }
            } else if (colType == Board::CollisionType::Right) {
                if (piecePoints.at(row).at(col) == 1) {
                    if (matrixCol == m_boardMatrix[0].size() - 1) {
                        return true;
                    }
                    if (m_boardMatrix.at(matrixRow).at(matrixCol + 1).first == 1) {
                        return true;
                    }
                }
            } else if (colType == Board::CollisionType::BlackHole) {
                if (piecePoints.at(row).at(col) == 1) {
                    if (matrixCol == m_black_hole.first && matrixRow == m_black_hole.second) {
                        return true;
                    }
                }

            }
        }
    }

    return false;
}

void Board::StickFallingPiece() {
    std::vector<std::vector<int>> piecePoints = m_fallingPiece->GetPoints();
    std::pair<int, int> pieceCoords = m_fallingPiece->GetCoords();
    for (int row = 0; row < piecePoints.size(); row++) {
        bool checkLineForDot = false;
        int matrixRow = pieceCoords.second + row;
        for (int col = 0; col < piecePoints[0].size(); col++) {
            int matrixCol = pieceCoords.first + col;
            if (piecePoints.at(row).at(col) == 1) {
                m_boardMatrix.at(matrixRow).at(matrixCol) = std::pair<int, sf::Color>(1, m_fallingPiece->GetColor());
                checkLineForDot = true;
            }
        }
        if (checkLineForDot) {// check matrix line if needed to clean
            CheckAndClearLine(matrixRow);
        }

    }
}

void Board::MoveRight() {
    if (!Colliding(Right))
        m_fallingPiece->MoveRight();

}

void Board::MoveLeft() {
    if (!Colliding(Left))
        m_fallingPiece->MoveLeft();
}

void Board::DropDown() {
    if (Colliding(Board::CollisionType::BlackHole)) {
        SliceFallingPiece();
    }
    if (!Colliding(Bottom))
        m_fallingPiece->MoveDown();
}

void Board::Rotate() {
    m_fallingPiece->Rotate();
    std::vector<std::vector<int>> points = m_fallingPiece->GetPoints();
    std::pair<int, int> pCoords = m_fallingPiece->GetCoords();

    if (pCoords.first < 0) {
        bool outOfBoard = false;
        bool outOfBoard2 = false;
        for (auto row:points) {
            if (pCoords.first == -2) {
                if (row.at(1) == 1) {
                    outOfBoard2 = true;
                    break;
                }
            }
            if (row.at(0) == 1) {
                outOfBoard = true;
            }

        }
        if (outOfBoard2) {
            m_fallingPiece->MoveRight();
            m_fallingPiece->MoveRight();
        } else if (outOfBoard) {
            m_fallingPiece->MoveRight();
        }
    }
    unsigned long rightLimit = m_boardMatrix.at(0).size() - 3;
    if (pCoords.first >= rightLimit) {
        bool outOfBoard = false;
        bool outOfBoard2 = false;
        for (auto row:points) {
            if (pCoords.first == m_boardMatrix.at(0).size() - 2) {
                if (row.at(row.size() - 2) == 1) {
                    outOfBoard2 = true;
                    break;
                }
            }
            if (row.at(row.size() - 1) == 1) {
                outOfBoard = true;
            }
        }
        if (outOfBoard2) {
            m_fallingPiece->MoveLeft();
            m_fallingPiece->MoveLeft();
        } else if (outOfBoard) {
            m_fallingPiece->MoveLeft();
        }
    }
}

int Board::GetScore() {
    return m_score;
}

void Board::SliceFallingPiece() {
    unsigned int colIndexToDel = m_black_hole.first - m_fallingPiece->GetCoords().first;
    unsigned int rowIndexToDel = m_black_hole.second - m_fallingPiece->GetCoords().second;
    m_fallingPiece->DeleteDot(rowIndexToDel, colIndexToDel);
    std::cout << rowIndexToDel << colIndexToDel;
}

