//
// Created by David on 2020. 12. 20..
//

#ifndef ECHIPA_25_FRONTEND_H
#define ECHIPA_25_FRONTEND_H
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "auxClasses/PieceQueue.h"
#include "Board.h"
#include "FileManager.h"

extern int nextID;

class Frontend {
public:
    sf::RenderWindow window;
    enum class GameState {
        StartMenu,
        SizeOption,
        InGameSP,
        InGameMP,
        MultiPlayerOption,
        MultiPlayerCoop,
        MultiPlayerVS,
        Escape,
    };
    std::vector<std::pair<sf::RectangleShape, sf::Text>> Buttons;
    sf::Font font;
    GameState gameState=GameState::StartMenu;
    void createWindow();
    void Game(FileManager &File);
    void InitButtons();
    void StartMenu();
    void InGame();
    sf::Texture RenderPieceQueue();
    int sizeX, sizeY;
};


#endif //ECHIPA_25_FRONTEND_H
