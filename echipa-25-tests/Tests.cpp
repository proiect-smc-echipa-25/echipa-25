//
// Created by Aglitoiu Marius on 17/01/2021.
//
#include "gtest/gtest.h"
#include "Frontend.h"
#include "Player.h"
#include "Board.h"

TEST(Tests, StartMenu) {
    Frontend *frontend = new Frontend;
    Frontend::GameState gameState = frontend->gameState;

    EXPECT_EQ(gameState, Frontend::GameState::StartMenu);
}

TEST(Tests, PlayerName){
    Player player;
    player.SetPlayerName("George");

    EXPECT_EQ(player.GetPlayerName(),"George");
}

TEST(Tests, PlayerScore){
    Player player;
    player.SetScore(500);

    EXPECT_EQ(player.GetScore(),500);
}

/*TEST(Tests, PlayerId){
    Player player;

    EXPECT_EQ(player.GetId(),!nullptr);
}NEEDS REVIEW*/

TEST(Tests, BoardScore){
    Board *board = new Board(30,10);
    Player player;
    player.SetScore(400);
    player.SaveScore();

    EXPECT_EQ(board->GetScore(),400);
}

TEST(Tests, GamesPlayed){
    Board *board = new Board(30,10);
    Player player;
    player.SetScore(400);
    player.SaveScore();
    board->GameOver();

    EXPECT_EQ(player.GetGamesPlayed(),1);
}




