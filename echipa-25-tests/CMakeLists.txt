cmake_minimum_required(VERSION 3.16)
project(echipa-25-tests)
set(GCC_COVERAGE_COMPILE_FLAGS "−std=gnu++11")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}" )

add_subdirectory(lib)
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
add_executable(RUN_TESTS Tests.cpp)

include_directories(${SFML_INCLUDE_DIR})

# linking Google_Tests_run with DateConverter_lib which will be tested
target_link_libraries(RUN_TESTS echipa-25-src)

target_link_libraries(RUN_TESTS gtest gtest_main)
